## Execução

Inicialmente o projeto esta contido no diretório "backend"

    cd backend

** todos os comandos desse tutorial devem ser feitos no diretório "backend"

## Docker

##### Server
Para a execução utilizando docker é necessario tem docker e docker-compose instalados

para executar a aplicação:

        sudo docker-compose up 

Após isso o servidor vai estar sendo executado em localhost:8000 ou 127.0.0.1:8000

##### Testes

Para a execução dos testes com docker:

        sudo docker-compose run web python3 manage.py test

## Python 

#### Necessário

* python3
* sudo pip3 install Django
* sudo pip3 install djangorestframework 

##### Server

para executar a aplicação:

        python3 manage.py runserver 

Após isso o servidor vai estar sendo executado em localhost:8000 ou 127.0.0.1:8000

##### Testes

Para a execução dos testes:

        python3 manage.py test


