from django.conf.urls import url
from . import views
from django.http import HttpResponse

urlpatterns = [
    url(r'^$', lambda request: HttpResponse('Check possible routes in https://gitlab.com/Zerum/backend-exam/blob/master/README.md'), name='hello'),

    url(r'^users$', views.UserList.as_view(), name='user-list'),
    url(r'^users/(?P<pk>[0-9]+)$', views.UserDetail.as_view(), name='user-detail'),

    url(r'^tasks$', views.TaskList.as_view(), name='task-list'),
    url(r'^tasks/(?P<pk>[0-9]+)$', views.TaskDetail.as_view(), name='task-detail'),

    url(r'^users/(?P<pk>[0-9]+)/tasks$', views.TaskUserDetail.as_view()),
]