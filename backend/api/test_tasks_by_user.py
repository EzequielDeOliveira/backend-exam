from rest_framework.test import APITestCase, APIClient
from rest_framework import status
from django.test import TestCase
from django.urls import reverse, resolve
from . import views
from .models import User, Task
import json


class Task_By_User(APITestCase):
    def test_tasks_by_user(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('task-list')
        data = {
            'user_id': '1',
            'description': 'first description for test'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('task-list')
        data = {
            'user_id': '1',
            'description': 'second description for test'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get('/users/1/tasks')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 2)
