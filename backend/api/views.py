from rest_framework import generics
from .models import User, Task
from .serializers import UserSerializer, TaskSerializer


class UserList(generics.ListCreateAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer


class TaskList(generics.ListCreateAPIView):

    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskUserDetail(generics.ListAPIView):

    serializer_class = TaskSerializer

    def get_queryset(self):

        user_id = self.kwargs['pk']
        return Task.objects.filter(user_id=user_id)
