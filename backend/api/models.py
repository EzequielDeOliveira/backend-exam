from django.db import models
from django.core.validators import MinValueValidator


# Create your models here.

class User(models.Model):
    USER_CHOICES = (
        ('admin', 'admin'),
        ('manager', 'manager'),
        ('default', 'default')
    )
    class Meta:

        db_table = 'user'

    name = models.CharField(max_length=200, blank=False, null=False)
    email = models.EmailField(unique=True, blank=False, null=False)
    age = models.PositiveIntegerField(
        default=18, validators=[MinValueValidator(18)], blank=False, null=False)
    user_type = models.CharField(
        max_length=10, choices=USER_CHOICES, default="admin", blank=False, null=False)

    def __str__(self):
        return self.email


class Task(models.Model):
    class Meta:

        db_table = 'task'

    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=50)
