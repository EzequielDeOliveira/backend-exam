from rest_framework.test import APITestCase, APIClient
from rest_framework import status
from django.test import TestCase
from django.urls import reverse, resolve
from . import views
from .models import User, Task


class User_Test(APITestCase):
    def test_create_user(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().name, 'Ezequiel')

    def test_List_user(self):
        response = self.client.get('/users')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_especific_user(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')
        response = self.client.get('/users/1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_user(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')
        new_data = {
            'name': 'Oliveira',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.put('/users/1', new_data, format='json')
        self.assertEqual(User.objects.get().name, 'Oliveira')

    def test_delete_user(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')
        response = self.client.delete('/users/1')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(User.objects.count(), 0)

