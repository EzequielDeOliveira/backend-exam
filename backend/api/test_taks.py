from rest_framework.test import APITestCase, APIClient
from rest_framework import status
from django.test import TestCase
from django.urls import reverse, resolve
from . import views
from .models import User, Task

class Task_Test(APITestCase):
    def test_create_task(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')

        url = reverse('task-list')
        data = {
            'user_id': '1',
            'description': 'description for test'
        }
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Task.objects.count(), 1)

    def test_List_task(self):
        response = self.client.get('/tasks')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_especific_task(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')

        url = reverse('task-list')
        data = {
            'user_id': '1',
            'description': 'description for test'
        }
        response = self.client.post(url, data, format='json')
        response = self.client.get('/tasks/1')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_task(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')

        url = reverse('task-list')
        data = {
            'user_id': '1',
            'description': 'description for test'
        }
        response = self.client.post(url, data, format='json')

        new_data = {
            'user_id': '1',
            'description': 'description updated'
        }
        response = self.client.put('/tasks/1', new_data, format='json')
        self.assertEqual(Task.objects.get().description, 'description updated')

    def test_delete_task(self):
        url = reverse('user-list')
        data = {
            'name': 'Ezequiel',
            'email': 'ezequiel1de1oliveira@gmail.com',
            'age': 20,
            'user_type': 'admin'
        }
        response = self.client.post(url, data, format='json')

        url = reverse('task-list')
        data = {
            'user_id': '1',
            'description': 'description for test'
        }
        response = self.client.post(url, data, format='json')

        response = self.client.delete('/tasks/1')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Task.objects.count(), 0)

    
